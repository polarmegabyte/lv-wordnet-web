const express = require('express');
const app = express();
const path = require('path');
const process = require('./process');
const cookieParser = require('cookie-parser');
const { I18n } = require('i18n');


app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');

const i18n = new I18n({
  // locales: ['en', 'lv'],
  // defaultLocale: 'lv',
  locales: ['en'],
  defaultLocale: 'en',
  cookie: 'locale',
  objectNotation: true,
  directory: path.join(__dirname, 'locales'),
  updateFiles: false
});
app.use(cookieParser());
app.use(i18n.init);
app.use(function(req, res, next) { // add i18n support to pug
  res.locals.__ = res.__ = function() {
    return i18n.__.apply(req, arguments);
  };
  next();
});

app.get('/:lang(en|lv)', (req, res) => {
  res.cookie('locale', req.params.lang, { maxAge: 900000, httpOnly: true });
  res.redirect('back');
});

app.get('/', (req, res) => {
  res.render('index', {name: 'index'});
});

app.get('/data', (req, res) => {
  res.render('data', {name: 'data'});
});

app.get('/demo', (req, res) => {
  res.render('demo', {name: 'demo'});
});

app.get('/wordlist', (req, res) => {
  res.render('wordlist', {name: 'wordlist'});
});

app.get('/publications', (req, res) => {
  let publications = process.getPublications();
  res.render('publications', {publications, name: 'publications'})
});


app.listen('9100', () => {
  console.log('Server on port 9100');
});

