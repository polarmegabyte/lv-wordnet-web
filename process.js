const Cite = require('citation-js');
const fs = require('fs');
const {data} = require("./publications.json");
var cite = new Cite();


function getPublications() {
  var bib = fs.readFileSync('publications_bib.txt', { encoding: 'utf8' });
  bib = bib.split("@");
  bib.shift();
  let bib_data = bib.map(b => {
    if (b !== '')
      return {...JSON.parse(cite.set('@'+ b).get({
        format: 'string',
        style: 'csl',
        lang: 'en-US'
      }))[0], ...{bib: '@' + b}};
  });
  let {data} = require('./publications.json');
  let all_data = [...bib_data, ...data];
  all_data.sort((a, b) => b.issued['date-parts'][0][0] - a.issued['date-parts'][0][0]);

  return all_data;
}


module.exports = {
  getPublications
}
