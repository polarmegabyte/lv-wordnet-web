import csv
import json

wordlist = {}

translations = {
    "Lietvārds": "noun",
    "Darbības vārds": "verb",
    "Īpašības vārds": "adjective",
    "Apstākļa vārds": "adverb",
    "Partikula": "particle",
    "Reziduālis": "foreign",
    "Skaitļa vārds": "numeral",
    "UL": None # null
}

def trans(pos):
    return translations[pos[1:-1]]

with open("wordlist_031022.csv", "r") as f:
    reader = csv.reader(f)
    for no, entry, entry_id, human_key, pos in reader:
        if no not in wordlist:
            wordlist[no] = {
                "no": no,
                "entry": entry,
                "links": [human_key],
                "pos": [trans(pos)] if trans(pos) else []
            }
            if not trans(pos):
                print(human_key, 'šķirklim nav vārdšķiras')
        else:
            if human_key not in wordlist[no]["links"]:
                wordlist[no]["links"].append(human_key)
            if trans(pos) not in wordlist[no]["pos"] and trans(pos):
                wordlist[no]["pos"].append(trans(pos))

print(len(wordlist))
json.dump(wordlist, open("wordlist.json", "w"))