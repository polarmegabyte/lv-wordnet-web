var name = 'ēka_1';
// var name = 'liels_1';

var options = {
  "physics": {
    "barnesHut": {
      "gravitationalConstant": -5000,
      "springLength": 150,
      "springConstant": 0.005,
      "damping": 0.4
    },
    "minVelocity": 2,
    "enabled": true
  },
  interaction: {
    navigationButtons: true
  }
};

function addSynsetMenu() {
  let menu = document.getElementById('synset-list');
  Object.entries(synsetNames).forEach(o => {
    menu.insertAdjacentHTML('beforeend', `<li><a class="dropdown-item synset-option" data-name="${o[0]}">${o[1].sense}</a></li>`);
  });
  document.querySelectorAll('.synset-option').forEach((el) =>
    el.addEventListener('click', () => initGraph(el.getAttribute('data-name')))
  );
}

function setDisplayName(name) {
  document.querySelectorAll('.synset-name').forEach((el) => el.innerHTML = synsetNames[name].synset);
  document.querySelectorAll('.sense-name').forEach((el) => el.innerHTML = synsetNames[name].sense);
}

function initGraph(name) {
  setDisplayName(name);

  var container = document.getElementById("graph");
  var data = {
    nodes: new vis.DataSet(nodesData[name]),
    edges: new vis.DataSet(edgesData[name]),
  };
  var network = new vis.Network(container, data, options);
}

addSynsetMenu();
initGraph(name);

