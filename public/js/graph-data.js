var nodesData = {
  'ēka_1': [
    {
      "id": 355,
      "label": "{celtne_1, ēka_1}",
      "shape": "box",
      "color": "#7BE141",
      "type": "main",
      "depth": null,
      "is_entry": false
    },
    {
      "id": "02913152-n",
      "label": "{building, edifice}\na structure that has a roof\n and walls and stands more or less\n permanently in one place",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 317,
      "label": "{pils_2}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03878066-n",
      "label": "{palace, castle}\na large and stately mansion  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 353,
      "label": "{kambaris_1.1, istaba_3}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 354,
      "label": "{māja_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hypernym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 401,
      "label": "{viesnīca_1, hotelis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03542333-n",
      "label": "{hotel}\na building where travelers\n can pay for lodging and\n meals and other services  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 402,
      "label": "{motelis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 403,
      "label": "{pansija_2}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "similar",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 441,
      "label": "{naktsmītne_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hypernym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 647,
      "label": "{tirgusplacis_1, tirgus_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 648,
      "label": "{telpa_2}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hypernym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 649,
      "label": "{ututirgus_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 650,
      "label": "{centrāltirgus_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 651,
      "label": "{blusu tirgus_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 676,
      "label": "{pašvaldība_1.2}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 677,
      "label": "{ministrija_1.1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03770316-n",
      "label": "{ministry}\nbuilding where the business\n of a government department\n is transacted  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 678,
      "label": "{banka_1.1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02787772-n",
      "label": "{bank, bank_building}\na building in which the\n business of banking transacted",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1765,
      "label": "{dievnams_1, baznīca_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03028079-n",
      "label": "{church, church_building}\na place for public (especially Christian)\n worship",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1766,
      "label": "{templis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1767,
      "label": "{svētnīca_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 298,
      "label": "{skola_1, skolasnams_1, baznīca_4}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "08276720-n",
      "label": "{school}\nan educational institution",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": "04146050-n",
      "label": "{school, schoolhouse}\na building where young people\n receive education",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 299,
      "label": "{Gaismas pils_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hypernym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1202,
      "label": "{vidusskola_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1204,
      "label": "{augstskola_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1203,
      "label": "{universitāte_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "08286569-n",
      "label": "{university}\na large and diverse institution\n of higher learning created to\n educate for life and for a profession\n and to grant degrees  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": "08286163-n",
      "label": "{university}\nthe body of faculty and students at\n a university  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1841,
      "label": "{būve_3}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "hyponym",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 515,
      "label": "{dzīvoklis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "meronym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": "02726305-n",
      "label": "{apartment, flat}\na suite of rooms usually on\n one floor of an apartment house  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 351,
      "label": "{istaba_1, kambaris_1}",
      "shape": "box",
      "color": "#97C2FC",
      "type": "meronym",
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1842,
      "label": "{ēka_2, klēts_1}",
      "shape": "box",
      "color": "#C2F1A7",
      "type": "entry",
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02793495-n",
      "label": "{barn}\nan outlying farm building for\n storing grain or animal feed\n and housing farm animals  ",
      "shape": "box",
      "color": "#FB7E81",
      "type": "external_links",
      "depth": 0,
      "is_entry": true
    }
  ],
  'liels_1': [
    {
      "id": 30,
      "label": "{brangs_2, dižens_1, dižs_1, ievērojams_4, pamatīgs_2, prāvs_1, liels_1}",
      "shape": "box",
      "color": "#7BE141",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "main",
      "level": 10,
      "depth": null,
      "is_entry": false
    },
    {
      "id": "01382086-a",
      "label": "{large, big}\nabove average in size or numbe...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 266,
      "label": "{mačs_2, mazs_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "antonym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 265,
      "label": "{paliels_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "gradset",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1004,
      "label": "{milzīgs_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "gradset",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 2569,
      "label": "{gigantisks_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "gradset",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 31,
      "label": "{krietns_2.1, labs_8.1, liels_2, prāvs_2.1, pamatīgs_2.5}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 32,
      "label": "{garš_2, ilgs_2, krietns_2.3, labs_8.3, liels_3}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01437963-a",
      "label": "{long}\nprimarily temporal sense",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 33,
      "label": "{liels_4, krietns_4}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01510444-a",
      "label": "{bad, big}\nvery intense",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 34,
      "label": "{liels_7, pieaudzis_3.1}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01488616-a",
      "label": "{adult, big, full-grown, fully_grown, grown, grownup}\n(of animals) fully developed",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 35,
      "label": "{pieaugušais_1, liels_7.1}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "09605289-n",
      "label": "{adult, grownup}\na fully developed person from ...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 1003,
      "label": "{liels_1.3, plašs_1.4}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 1005,
      "label": "{liels_1.2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01471002-a",
      "label": "{major}\ngreater in number or size or a...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3065,
      "label": "{atbildīgs_1, nozīmīgs_1, liels_5, svarīgs_1, būtisks_2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01276872-a",
      "label": "{big}\nsignificant",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3161,
      "label": "{apjomīgs_1, liels_2.1}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "00014858-a",
      "label": "{copious, voluminous}\nlarge in number or quantity (e...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3607,
      "label": "{liels_4.3}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 3608,
      "label": "{vērtīgs_1, liels_5.1}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02500884-a",
      "label": "{valuable}\nhaving great material or monet...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3610,
      "label": "{liels_6}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02402439-a",
      "label": "{big(a), heavy(a)}\nprodigious",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3611,
      "label": "{ievērojams_2, izcils_1, liels_6.2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01278818-a",
      "label": "{great, outstanding}\nof major significance or impor...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3612,
      "label": "{bagāts_1, turīgs_1, liels_6.3}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02022167-a",
      "label": "{affluent, flush, loaded, moneyed, wealthy}\nhaving an abundant supply of m...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": "02021905-a",
      "label": "{rich}\npossessing material wealth",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 3613,
      "label": "{ietekmīgs_1, liels_5.4}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01830403-a",
      "label": "{authoritative, important}\nhaving authority or ascendancy...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    }
  ],
  'automobilis': [
    {
      "id": 3392,
      "label": "{automašīna_1, mašīna_2, vāģis_3, automobilis_1, auto_1, autiņš_3}",
      "shape": "box",
      "color": "#7BE141",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "main",
      "level": 10,
      "depth": null,
      "is_entry": false
    },
    {
      "id": "02958343-n",
      "label": "{car, auto, automobile, machine, motorcar}\na motor vehicle with four whee...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 190,
      "label": "{transportierīce_1, transportlīdzeklis_1, transports_1.1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hypernym",
      "level": 9,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "04524313-n",
      "label": "{vehicle}\na conveyance that transports p...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 9,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1295,
      "label": "{instruments_1, rīks_1, līdzeklis_1.2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hypernym",
      "level": 8,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 194,
      "label": "{vilciens_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hyponym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 2986,
      "label": "{autobuss_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hyponym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 2987,
      "label": "{tramvajs_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hyponym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3395,
      "label": "{virsbūve_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02863426-n",
      "label": "{bodywork}\nthe exterior body of a motor v...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3401,
      "label": "{lukturis_2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3402,
      "label": "{Vēja stikls_1, vējstikls_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3404,
      "label": "{bampers_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3571,
      "label": "{spārns_3}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3572,
      "label": "{logs_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3403,
      "label": "{bagāžnieks_2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3573,
      "label": "{Kravas kaste_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3574,
      "label": "{jumts_1.1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3399,
      "label": "{šasija_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03010473-n",
      "label": "{chassis}\nthe skeleton of a motor vehicl...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3400,
      "label": "{kardāns_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3562,
      "label": "{pārnesumkārba_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 1986,
      "label": "{Stūres iekārta_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3394,
      "label": "{bremze_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3566,
      "label": "{transmisija_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3567,
      "label": "{tilts_2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3397,
      "label": "{ritenis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3524,
      "label": "{motors_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "03789946-n",
      "label": "{motor}\nmachine that converts other fo...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3528,
      "label": "{virzulis_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3525,
      "label": "{vārsts_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3526,
      "label": "{cilindrs_2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3527,
      "label": "{svece_3}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3570,
      "label": "{starteris_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3568,
      "label": "{salons_4}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "08588294-n",
      "label": "{inside, interior}\nthe region that is inside of s...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3575,
      "label": "{panelis_3}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 3569,
      "label": "{akumulators_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "meronym",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "04328329-n",
      "label": "{storage_battery, accumulator}\na voltaic battery that stores ...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": false
    }
  ],
  'izdarīt_1': [
    {
      "id": 507,
      "label": "{izdarīt_1, nodarīt_3, padarīt_1, paveikt_1}",
      "shape": "box",
      "color": "#7BE141",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "main",
      "level": 10,
      "depth": null,
      "is_entry": false
    },
    {
      "id": "00484166-v",
      "label": "{complete, finish}\ncome or bring to a finish or a...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "01712704-v",
      "label": "{perform, execute, do}\ncarry out or perform an action",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 504,
      "label": "{darīt_1, veikt_1.2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "hypernym",
      "level": 9,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 660,
      "label": "{darīties_1}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "also",
      "level": 9,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 661,
      "label": "{ņemties_2}",
      "shape": "box",
      "color": "#97C2FC",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "similar",
      "level": 9,
      "depth": 0,
      "is_entry": false
    },
    {
      "id": 262,
      "label": "{iedzīvināt_1, īstenot_1, izdarīt_2, izpildīt_1, veikt_1, realizēt_1, piepildīt_6}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "00486018-v",
      "label": "{follow_through, follow_up, follow_out, carry_out, implement, put_through, go_through}\npursue to a conclusion or brin...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": "02526085-v",
      "label": "{achieve, accomplish, attain, reach}\nto gain with effort",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": "01644746-v",
      "label": "{realize, realise, actualize, actualise, substantiate}\nmake real or concrete",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 426,
      "label": "{nodarīt_1, izdarīt_1.3, pastrādāt_3, padarīt_2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02582615-v",
      "label": "{perpetrate, commit, pull}\nperform an act, usually with a...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 604,
      "label": "{izdarīt_1.2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": 611,
      "label": "{iesākt_5, pasākt_3, izdarīt_1.1, padarīt_1.2}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "02561995-v",
      "label": "{do, perform}\nget (something) done",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    },
    {
      "id": 623,
      "label": "{izdarīt_3}",
      "shape": "box",
      "color": "#C2F1A7",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "entry",
      "level": 10,
      "depth": 1,
      "is_entry": false
    },
    {
      "id": "00730758-v",
      "label": "{draw, make}\nmake, formulate, or derive in ...",
      "shape": "box",
      "color": "#FB7E81",
      "widthConstraint": {
        "maximum": 250
      },
      "type": "external_links",
      "level": 10,
      "depth": 0,
      "is_entry": true
    }
  ]
};

var edgesData = {
  'ēka_1': [
    {
      "from": 355,
      "to": "02913152-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "b40e6262-9f50-4f0c-84db-53633af58818"
    },
    {
      "from": 355,
      "to": 317,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "f2d204d6-4180-4e7a-94ba-f24d1db98212"
    },
    {
      "from": 317,
      "to": "03878066-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "00a31880-597a-4321-9e51-0fe46249bb76"
    },
    {
      "from": 355,
      "to": 353,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "8e1ff1a1-6fc2-4172-8cf3-212a1a7ff893"
    },
    {
      "from": 353,
      "to": 354,
      "label": "hypernym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "279e6dfd-33c1-411a-a26e-112e78e1d036"
    },
    {
      "from": 355,
      "to": 401,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "35a6a5a1-3ef2-4ff7-8c2e-b91f188a8b9c"
    },
    {
      "from": 401,
      "to": "03542333-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "1e409953-a833-428c-800d-329e45383c7a"
    },
    {
      "from": 401,
      "to": 402,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "b349f2c8-1c0f-4aca-bf08-a6d5724ef598"
    },
    {
      "from": 401,
      "to": 403,
      "label": "similar",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "664c38b5-00de-4bd4-b80f-c4a506453453"
    },
    {
      "from": 401,
      "to": 441,
      "label": "hypernym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "33aa8d1d-ae12-43ed-83ef-b8e916ead444"
    },
    {
      "from": 355,
      "to": 647,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "7a5571ba-8d39-4889-9c69-e63d6b985648"
    },
    {
      "from": 647,
      "to": 648,
      "label": "hypernym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "9683b7fe-35cf-4e70-b172-e074a59649f3"
    },
    {
      "from": 647,
      "to": 649,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "9b9791df-1a35-44a8-a361-bbf60bff8a2e"
    },
    {
      "from": 647,
      "to": 650,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "1caf25f2-04cb-430a-956d-89d3ac0bc4cc"
    },
    {
      "from": 647,
      "to": 651,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "cba241a6-4588-4280-9409-e7d47c686673"
    },
    {
      "from": 355,
      "to": 676,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "aa79e498-4d0b-4406-8ea9-46ecc40f95d9"
    },
    {
      "from": 355,
      "to": 677,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "cd5ddabf-e6bb-453d-b5fb-dfc332a7c8f6"
    },
    {
      "from": 677,
      "to": "03770316-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "ee1d3af4-d7c6-4349-a333-5c47baba2c1a"
    },
    {
      "from": 355,
      "to": 678,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "6d7d220a-b85b-49b0-b8d0-a2d29c156ae9"
    },
    {
      "from": 678,
      "to": "02787772-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "50a1827b-aaad-4f39-91e0-d5092fa1e8fa"
    },
    {
      "from": 355,
      "to": 1765,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "ec17432f-6818-4ea2-b1cb-f46ffc29633d"
    },
    {
      "from": 1765,
      "to": "03028079-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "b8d9fe26-2bf6-43db-bb88-01058b4929b9"
    },
    {
      "from": 1765,
      "to": 1766,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "7960ff9f-e82e-491f-875d-232639c252e0"
    },
    {
      "from": 1765,
      "to": 1767,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "f3a3bb94-0c82-4a96-9c3c-41452b02dbda"
    },
    {
      "from": 355,
      "to": 298,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "fcf2fb29-5ea1-4408-8e90-cee165086a33"
    },
    {
      "from": 298,
      "to": "08276720-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "794d1efa-13e8-48a6-86c5-42976b460b56"
    },
    {
      "from": 298,
      "to": "04146050-n",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a2a62bc6-14aa-4506-b71e-eb6c7b108b68"
    },
    {
      "from": 298,
      "to": 299,
      "label": "hypernym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "3d4f0094-3fd8-48e0-959f-e01457dd0139"
    },
    {
      "from": 298,
      "to": 1202,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "ca84d6a3-9fcc-46fb-83ad-66fba6ecb83e"
    },
    {
      "from": 298,
      "to": 1204,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "dbf22fef-fbbb-4c96-9e88-280166f3f90f"
    },
    {
      "from": 355,
      "to": 1203,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "a51ca760-ff48-44f5-9394-6905b102d770"
    },
    {
      "from": 1203,
      "to": "08286569-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "64a505c3-a262-4bd0-9d84-a94b7b0f424e"
    },
    {
      "from": 1203,
      "to": "08286163-n",
      "label": "PWN narrower",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "61c9d022-fbe0-4971-b6b6-bb40bd036fbb"
    },
    {
      "from": 1203,
      "to": 1204,
      "label": "hypernym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "85af1064-599b-4153-a2b6-371256fed6c0"
    },
    {
      "from": 355,
      "to": 1841,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "e1b802d8-a235-4d27-905c-77144a789d98"
    },
    {
      "from": 355,
      "to": 354,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "410cadd3-c42a-4dfe-85bb-2c83596ee868"
    },
    {
      "from": 354,
      "to": 515,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "0741a08c-505e-4a92-a836-80941915c6b8"
    },
    {
      "from": 355,
      "to": 515,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "4dd45447-46af-4d80-9739-06862f7b7ce0"
    },
    {
      "from": 515,
      "to": "02726305-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "22a5b49d-f193-4f52-9e0f-3fb70d9f457b"
    },
    {
      "from": 515,
      "to": 351,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "5e5b3791-c512-4c31-b1c5-559efcd21008"
    },
    {
      "from": 355,
      "to": 1842,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "e2573581-354f-4067-81bb-b2c48bdf3a44"
    },
    {
      "from": 1842,
      "to": "02793495-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "0e83a978-cc40-4a70-b763-3d92c03c3805"
    }
  ],
  'liels_1': [
    {
      "from": 30,
      "to": "01382086-a",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "1eb3eefc-6c2b-408f-aa28-6ba2805d4bf2"
    },
    {
      "from": 30,
      "to": 266,
      "label": "antonīms",
      "arrows": "to, from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "53b83db3-5cf3-4d2c-9e40-9c649912e31b"
    },
    {
      "from": 266,
      "to": 265,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "b632c6a5-7815-4c6e-b12d-da91a39d71ee"
    },
    {
      "from": 266,
      "to": 1004,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "aba8a5a4-0472-414d-b170-a57e3b4cadbf"
    },
    {
      "from": 266,
      "to": 2569,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "46167bcc-07ff-41d2-89cd-8e444e72c094"
    },
    {
      "from": 30,
      "to": 265,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "c44cdb1f-1ca7-4480-85a4-5e0700080184"
    },
    {
      "from": 265,
      "to": 1004,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "8351d5b5-f0b7-46c1-827f-50ca2018a4c5"
    },
    {
      "from": 265,
      "to": 2569,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "d077ffa4-4c93-4c57-b1b9-044c9e7c5417"
    },
    {
      "from": 30,
      "to": 1004,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "2225c20f-468d-473a-9e3f-0fc8e872b3d9"
    },
    {
      "from": 1004,
      "to": 2569,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "e4545fbd-ca5f-437f-87d3-a119ab7895ee"
    },
    {
      "from": 30,
      "to": 2569,
      "label": "gradset",
      "arrows": "",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "3b3ede37-356f-49d7-84b2-9f1c619af9f2"
    },
    {
      "from": 30,
      "to": 31,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "f8bf14ab-f575-4521-a431-4aa668cd58c6"
    },
    {
      "from": 31,
      "to": "01382086-a",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "58068192-e139-4ec7-b414-acbe83a0381a"
    },
    {
      "from": 30,
      "to": 32,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "23f6eda2-84cf-445c-89cc-5f3197870fc2"
    },
    {
      "from": 32,
      "to": "01437963-a",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "79e6a468-9366-4287-9960-aa63892b7d28"
    },
    {
      "from": 30,
      "to": 33,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "3a467152-ba44-4ed6-91f6-51972b3d4bd7"
    },
    {
      "from": 33,
      "to": "01510444-a",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "616bcaba-31c7-4ff3-a0ff-240402e86b63"
    },
    {
      "from": 30,
      "to": 34,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "3ca43cf9-759f-480b-8066-91c93d89d503"
    },
    {
      "from": 34,
      "to": "01488616-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "df304ad5-4bb1-4dd7-9250-82715aae533c"
    },
    {
      "from": 30,
      "to": 35,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "592dcf2f-0f81-4dd3-b5b5-644658eb0384"
    },
    {
      "from": 35,
      "to": "09605289-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "9c9fe290-92f2-4dce-9189-fd8c565c2025"
    },
    {
      "from": 30,
      "to": 1003,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "5318e2e7-9c32-48f3-9127-edcc272e9cf3"
    },
    {
      "from": 30,
      "to": 1005,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "05a78d58-f3c8-4e31-85ce-5de137f4e1e4"
    },
    {
      "from": 1005,
      "to": "01471002-a",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "d119f2ec-24cf-46e5-9328-8a88a4a8f3ad"
    },
    {
      "from": 30,
      "to": 3065,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "0686abd6-7dd5-44ce-9ce8-f1ee01ca703b"
    },
    {
      "from": 3065,
      "to": "01276872-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "91a39dba-7e01-4323-914a-14b3e55cff82"
    },
    {
      "from": 30,
      "to": 3161,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "cf809593-3e62-4536-afeb-83109a473a69"
    },
    {
      "from": 3161,
      "to": "00014858-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a3a80bdd-f2dd-4c60-a6a0-0aa86024e380"
    },
    {
      "from": 30,
      "to": 3607,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "ef0fa988-12c0-4e98-bb33-db34ae77c7fa"
    },
    {
      "from": 3607,
      "to": "01510444-a",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "7a48430a-8da6-4884-995b-6e98ee4d805f"
    },
    {
      "from": 30,
      "to": 3608,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "64a22e57-bc5e-489d-8229-fe5e3c487013"
    },
    {
      "from": 3608,
      "to": "02500884-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "c0b978f8-0c2b-4643-b032-eb7d1704af8a"
    },
    {
      "from": 30,
      "to": 3610,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "605aa4e7-5c0f-491a-98ab-8f85ae2b1509"
    },
    {
      "from": 3610,
      "to": "02402439-a",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "879a9240-0ed2-4bfc-a013-9809814afd08"
    },
    {
      "from": 30,
      "to": 3611,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "75f312fa-b22d-49d0-8ba9-94e07cf198b1"
    },
    {
      "from": 3611,
      "to": "01278818-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "20565cd7-91d6-41b4-a10c-2c1364a9c6b9"
    },
    {
      "from": 30,
      "to": 3612,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "bd67ef8d-600a-4883-b2a2-8f853c4c743f"
    },
    {
      "from": 3612,
      "to": "02022167-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "e5abdfa3-43b0-4fb9-a794-3e2af2e744c0"
    },
    {
      "from": 3612,
      "to": "02021905-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "28cfac89-9d86-4f37-a9a9-397648bc66da"
    },
    {
      "from": 30,
      "to": 3613,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "9f29aefc-d635-4afa-8228-b2f28910991e"
    },
    {
      "from": 3613,
      "to": "01830403-a",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "ebcd21a5-fa0f-4167-ae7a-64cb2e666c1e"
    }
  ],
  'automobilis': [
    {
      "from": 3392,
      "to": "02958343-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a02f50c1-30e6-47af-90ce-51b44c677c64"
    },
    {
      "from": 3392,
      "to": 190,
      "label": "hyponym",
      "arrows": "from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "1f5c2745-b6de-480b-bc47-9a00d75b6fb1"
    },
    {
      "from": 190,
      "to": "04524313-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "6f92d3f4-cb69-411a-95f0-9ebfa1d0f655"
    },
    {
      "from": 190,
      "to": 1295,
      "label": "hyponym",
      "arrows": "from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "4c969e20-13d7-44ca-99bd-c7df1d45a1fb"
    },
    {
      "from": 190,
      "to": 194,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "85cf9d52-d4b7-4776-9235-631716cc3939"
    },
    {
      "from": 190,
      "to": 2986,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "de9960f1-d3b7-4686-b03e-b6b81991dfb2"
    },
    {
      "from": 190,
      "to": 2987,
      "label": "hyponym",
      "arrows": "to",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "0ad551af-0325-461d-884a-80a504502c84"
    },
    {
      "from": 3392,
      "to": 3395,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "6d9c248e-05d8-4dad-bc9c-804f920b79bd"
    },
    {
      "from": 3395,
      "to": "02863426-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a77d4600-7e11-44d0-a355-c523a8b3ba92"
    },
    {
      "from": 3395,
      "to": 3401,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "9703412d-e7de-4941-88c6-280b9a42f5c7"
    },
    {
      "from": 3395,
      "to": 3402,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "969564b1-117d-4d68-9f58-3ba99fd42c75"
    },
    {
      "from": 3395,
      "to": 3404,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "7b175b1e-2840-45b8-88cf-ee8c4152e609"
    },
    {
      "from": 3395,
      "to": 3571,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "de82ff47-0c3f-4140-9386-45a9135b833c"
    },
    {
      "from": 3395,
      "to": 3572,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "137a9f0f-479a-471e-adda-6b874b889565"
    },
    {
      "from": 3395,
      "to": 3403,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "f616256a-5dfb-444f-a22a-1de7d947f545"
    },
    {
      "from": 3395,
      "to": 3573,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "06ce2a9e-ffa7-4bdd-8f73-764613e8cf24"
    },
    {
      "from": 3395,
      "to": 3574,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "b04576c7-2695-4c15-a1fe-a0a51138e764"
    },
    {
      "from": 3392,
      "to": 3399,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "bcb27b34-25ec-469c-a905-93aa2b28789b"
    },
    {
      "from": 3399,
      "to": "03010473-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "ec61f435-16f8-48c1-bebc-ef1a3ac2c7bc"
    },
    {
      "from": 3399,
      "to": 3400,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "af2177ec-e1bd-4b55-9a07-fc030bf82e18"
    },
    {
      "from": 3399,
      "to": 3562,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "60c9ec00-24e0-4f3f-aa91-377def377011"
    },
    {
      "from": 3399,
      "to": 1986,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "d10c77b5-277d-4d87-9b76-c2a573fda6e5"
    },
    {
      "from": 3399,
      "to": 3394,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "06bec712-0a94-4d66-8dab-744f8218aae6"
    },
    {
      "from": 3399,
      "to": 3566,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "c0bdfeb7-8204-43ce-854c-2eacc1430a24"
    },
    {
      "from": 3399,
      "to": 3567,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "9323174a-2b50-41ab-acfd-db2fb4fa813e"
    },
    {
      "from": 3399,
      "to": 3397,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "5e73f014-e835-4fa0-8197-c6d283d748ea"
    },
    {
      "from": 3392,
      "to": 3524,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "5d29bfd2-fc91-43d4-9442-24f9335bf29b"
    },
    {
      "from": 3524,
      "to": "03789946-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "9aedf21e-ffc2-486a-8b1b-caa6f4ea49e2"
    },
    {
      "from": 3524,
      "to": 3528,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "170c8d76-5aec-4499-ae60-8e3e82751710"
    },
    {
      "from": 3524,
      "to": 3525,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "5cb5ff24-9329-4d1a-aecb-3dd6a9c175c3"
    },
    {
      "from": 3524,
      "to": 3526,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "bf6de75a-8372-46cd-b752-d61e0d801595"
    },
    {
      "from": 3524,
      "to": 3527,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "8128ac4f-3e87-4a0f-865a-4acdda6414b1"
    },
    {
      "from": 3524,
      "to": 3570,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "be5d6164-9375-4950-ac09-05370f4d5945"
    },
    {
      "from": 3392,
      "to": 3568,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "8963142a-59ca-4a45-b688-4475f557f6c0"
    },
    {
      "from": 3568,
      "to": "08588294-n",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "4ca80830-b8ef-4895-aafc-ea80745e401a"
    },
    {
      "from": 3568,
      "to": 3575,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "8bcb5349-ce7b-485d-ba83-e69dffdccc1f"
    },
    {
      "from": 3392,
      "to": 3569,
      "label": "meronym",
      "arrows": "to",
      "dashes": false,
      "color": "#FCD197",
      "font": {
        "align": "top"
      },
      "id": "495cb6de-df6b-4cca-8548-b2f360cc1f29"
    },
    {
      "from": 3569,
      "to": "04328329-n",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "3b260a36-71f7-432b-9deb-5416f141c9c0"
    }
  ],
  'izdarīt_1': [
    {
      "from": 507,
      "to": "00484166-v",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "94f60576-5ceb-4fa9-b339-7a7d7eb9173c"
    },
    {
      "from": 507,
      "to": "01712704-v",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "80bc2edc-2a90-40c0-88bb-74cdcb193dda"
    },
    {
      "from": 507,
      "to": 504,
      "label": "hyponym",
      "arrows": "from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "5c5ea6ef-d2d7-4560-987c-2414b8f7be7e"
    },
    {
      "from": 504,
      "to": "01712704-v",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "b02bec78-d854-42d3-ad3a-00e4859012f8"
    },
    {
      "from": 504,
      "to": 660,
      "label": "also",
      "arrows": "to, from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "52f2c0a5-eea1-49d9-8c50-694463d4cbd6"
    },
    {
      "from": 504,
      "to": 661,
      "label": "similar",
      "arrows": "to, from",
      "dashes": false,
      "color": "#97C2FC",
      "font": {
        "align": "top"
      },
      "id": "dd4cefc2-0390-4507-a52c-8e9ea7ff5c64"
    },
    {
      "from": 507,
      "to": 262,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "6441162b-0f77-4c27-ab7f-9c65336c15da"
    },
    {
      "from": 262,
      "to": "00486018-v",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "4c5746bd-398b-490c-9180-fe0038e2f7c2"
    },
    {
      "from": 262,
      "to": "02526085-v",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "09acf021-00af-4723-8563-f9eb7744df1f"
    },
    {
      "from": 262,
      "to": "01644746-v",
      "label": "PWN",
      "arrows": {
        "from": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a2d10750-c9e9-4fec-bf98-d43a1c393bdd"
    },
    {
      "from": 507,
      "to": 426,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "0fb2a055-da01-4106-b7cb-811a556834f2"
    },
    {
      "from": 426,
      "to": "02582615-v",
      "label": "PWN",
      "arrows": "",
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "e565ff19-7ebd-4c67-bbd1-4374832f08d4"
    },
    {
      "from": 507,
      "to": 604,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "f7040b0b-e6d9-4d9c-841b-d17f526c4e92"
    },
    {
      "from": 507,
      "to": 611,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "5605b390-599c-44f6-8cc1-218cb5428498"
    },
    {
      "from": 611,
      "to": "02561995-v",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "ab707520-e9d7-415e-b720-3d4f56ca52ab"
    },
    {
      "from": 507,
      "to": 623,
      "label": "word sense",
      "arrows": "",
      "dashes": true,
      "color": "#C2F1A7",
      "font": {
        "align": "top"
      },
      "id": "9f4a401e-e843-40f3-90fb-f6b36238df3d"
    },
    {
      "from": 623,
      "to": "00730758-v",
      "label": "PWN",
      "arrows": {
        "to": {
          "enabled": true,
          "type": "inv_triangle"
        }
      },
      "dashes": false,
      "color": "#FB7E81",
      "font": {
        "align": "top"
      },
      "id": "a8bf7f41-ef38-4752-a379-d5d231cf2f0b"
    }
  ]
};

var synsetNames = {
  'ēka_1': {sense: 'ēka<sub>1</sub>',  synset: '{celtne<sub>1</sub>, ēka<sub>1</sub>}'},
  'liels_1': {sense: 'liels<sub>1</sub>', synset: '{brangs<sub>2</sub>, dižens<sub>1</sub>, dižs<sub>1</sub>, ievērojams<sub>4</sub>, pamatīgs<sub>2</sub>, prāvs<sub>1</sub>, liels<sub>1</sub>}'},
  'automobilis': {sense: 'automobilis<sub>1</sub>', synset: '{automašīna<sub>1</sub>, mašīna<sub>2</sub>, vāģis<sub>3</sub>, automobilis<sub>1</sub>, auto<sub>1</sub>, autiņš<sub>3</sub>}'},
  'izdarīt_1': {sense: 'izdarīt<sub>1</sub>', synset: '{izdarīt<sub>1</sub>, nodarīt<sub>3</sub>, padarīt<sub>1</sub>, paveikt<sub>1</sub>}'}
};