var copyBtns = document.querySelectorAll('.copy-btn');
copyBtns.forEach(btn => {
  btn.addEventListener('click', clipboardCopy);
});
async function clipboardCopy(e) {
  let btn = e.currentTarget;
  btn.classList.add('btn-success');
  var i = btn.getAttribute("data-i");
  var text = document.getElementById("text"+i).innerHTML;
  await navigator.clipboard.writeText(text).then(() => {
    setTimeout(() => btn.classList.remove('btn-success'), 2000);
  });
}