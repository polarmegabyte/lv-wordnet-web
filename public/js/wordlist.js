var collator = new Intl.Collator('lv');

new gridjs.Grid({
  columns: [{
    id: 'entry',
    name: 'Word',
    sort: {
      compare: (a, b) => {
        let i = 0;
        while (i < a.length && i < b.length) {
          const aChar = a.charAt(i);
          const bChar = b.charAt(i);
          if (aChar !== bChar) {
            return collator.compare(aChar, bChar);
          }
          i++;
        }
      }
    }
  }, {
    id: 'links',
    name: 'Links to Tēzaurs.lv',
    sort: {
      enabled: false
    },
    data: (row) => row.links.join(','),
    formatter: (cell) => gridjs.html(cell.split(',').sort().map((link) => `<a href="https://tezaurs.lv/${encodeURIComponent(link)}" target="_blank">https://tezaurs.lv/${link}</a>`).join('<br>'))
  }, {
    id: 'pos',
    name: 'Part of Speech',
    data: (row) => row.pos.join(', ')
  }],
  pagination: true,
  search: true,
  sort: true,
  language: {
    'search': {
      'placeholder': 'Search...'
    }
  },
  style: {
    td: {
      padding: "6px 24px"
    }
  },
  data: wordlistData
}).render(document.getElementById("wrapper"));